#!/bin/bash
. ./test_common.sh

mkdir -p "$TEST_DIR"/{mount1,mount2-full,mount3}

check "Adding mount1" 0 "Mount point added." ./hddreplicas.py mount add "$TEST_DIR"/mount1
check "Adding mount2" 0 "Mount point added." ./hddreplicas.py mount add "$TEST_DIR"/mount2-full
check "Adding mount3" 0 "Mount point added." ./hddreplicas.py mount add "$TEST_DIR"/mount3

check "Adding HDD1" 0 "HDD added." ./hddreplicas.py hdd add 1 HDD1
check "Adding HDD2" 0 "HDD added." ./hddreplicas.py hdd add 2 HDD2
check "Adding HDD3" 0 "HDD added." ./hddreplicas.py hdd add 3 HDD3

tar cf "$TEST_DIR"/multi.tar "$TEST_DIR"/mount{1,2,3}* "$TEST_DIR"/database.sqlite

echo file1 > "$TEST_DIR"/mount1/file1
echo file2 > "$TEST_DIR"/mount1/file2
echo file1 > "$TEST_DIR"/mount2-full/file1

UPDATE_LOG="Updating files state of HDD1...
Hashing file file1
Hashing file file2
Updating files state of HDD2...
Hashing file file1
Updating files state of HDD3...
Handling stale metadata...
Replicating files...
Copying file2 from \"HDD1\" to \"HDD3\""

check "Replicate to disk with available space" 0 "$UPDATE_LOG" ./hddreplicas.py update

FILES_LOG="Id  Filename  HDD   Size   Md5
--  --------  ----  -----  -------
1   file1     HDD1  6.0 B  5149d40
3   file1     HDD2  6.0 B  5149d40
2   file2     HDD1  6.0 B  3d709e8
4   file2     HDD3  6.0 B  3d709e8

Count: 4"

check "Replicate to disk with available space, files list" 0 "$FILES_LOG" ./hddreplicas.py files search .


rm -rf "$TEST_DIR"/mount{1,2,3}* "$TEST_DIR"/database.sqlite
tar xf "$TEST_DIR"/multi.tar
mkdir "$TEST_DIR"/{mount1,mount2-full,mount3}/sub
echo file1 > "$TEST_DIR"/mount1/sub/file1
echo file1 > "$TEST_DIR"/mount2-full/sub/file1
echo file1 > "$TEST_DIR"/mount3/sub/file1

UPDATE_LOG="Updating files state of HDD1...
Hashing file sub/file1
Updating files state of HDD2...
Hashing file sub/file1
Updating files state of HDD3...
Hashing file sub/file1
Handling stale metadata...
Deleting sub/file1 from HDD3
Removing empty directory sub from HDD3
Replicating files..."

check "Extra replicas" 0 "$UPDATE_LOG" ./hddreplicas.py update

FILES_LOG="Id  Filename   HDD   Size   Md5
--  ---------  ----  -----  -------
1   sub/file1  HDD1  6.0 B  5149d40
2   sub/file1  HDD2  6.0 B  5149d40

Count: 2"

check "Extra replicas, files list" 0 "$FILES_LOG" ./hddreplicas.py files search .
check "Extra replicas, file is deleted" 0 "" test ! -e "$TEST_DIR"/mount3/sub/file1
check "Extra replicas, parent dir is deleted" 0 "" test ! -e "$TEST_DIR"/mount3/sub
check "Extra replicas, delete file db entry is deleted" 1 "No such file exists (id 3)." ./hddreplicas.py debug show_file 3


rm -rf "$TEST_DIR"/mount{1,2,3}* "$TEST_DIR"/database.sqlite
tar xf "$TEST_DIR"/multi.tar
mkdir "$TEST_DIR"/{mount1,mount2-full,mount3}/sub
echo file1 > "$TEST_DIR"/mount1/sub/file1
echo xxxxx > "$TEST_DIR"/mount2-full/sub/file1

HASH_CONFLICT="Updating files state of HDD1...
Hashing file sub/file1
Updating files state of HDD2...
Hashing file sub/file1
*** Hash conflict ***: file \"sub/file1\" on HDD2 has a different hash (02de18d4addc78e6b963c729de8bc0b6), than already indexed file(s):
Id  Filename   HDD   Size   Md5
--  ---------  ----  -----  -------
1   sub/file1  HDD1  6.0 B  5149d40

Count: 1

Please, overwrite the files with faulty hashes, or delete all replicas to resolve the conflict."

check "Files hash conflict" 1 "$HASH_CONFLICT" ./hddreplicas.py update
check "Files hash conflict, conflict is not indexed" 1 "No such file exists (id 2)." ./hddreplicas.py debug show_file 2


rm -rf "$TEST_DIR"/mount{1,2,3}* "$TEST_DIR"/database.sqlite
tar xf "$TEST_DIR"/multi.tar
dd if=/dev/zero of="$TEST_DIR"/mount1/file1 bs=1000 count=1 &>/dev/null
rm -rf "$TEST_DIR"/mount3/* "$TEST_DIR"/mount3/.*

FREESPACE_OTHER_DISK="Updating files state of HDD1...
Hashing file file1
Updating files state of HDD2...
Handling stale metadata...
Replicating files...

1 files are missing replicas, some replicas could be done if the following disks are mounted together:

Disks          Pending replicas
-------------  ----------------
HDD1 and HDD3  1"

check "Free space on other disk" 1 "$FREESPACE_OTHER_DISK" ./hddreplicas.py update


rm -rf "$TEST_DIR"/mount{1,2,3}* "$TEST_DIR"/database.sqlite
tar xf "$TEST_DIR"/multi.tar
dd if=/dev/zero of="$TEST_DIR"/mount1/file1 bs=1001 count=1 &>/dev/null

NO_FREESPACE="Updating files state of HDD1...
Hashing file file1
Updating files state of HDD2...
Updating files state of HDD3...
Handling stale metadata...
Replicating files...

1 files can't be copied due to missing free space.
Please add a new disk or delete some files."

check "No free space" 1 "$NO_FREESPACE" ./hddreplicas.py update
