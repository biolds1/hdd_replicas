#!/bin/bash
EXIT_ON_FAILURE=1
SKIP_ERROR_CHECKS=0
TEST_DIR=tests
rm -rf "$TEST_DIR"
mkdir -p "$TEST_DIR"
export HDD_REPLICAS_CONFIG_FILE="$TEST_DIR/config"
export HDD_REPLICAS_UNDER_TEST=1

function check() {
    title="$1"
    expected_status="$2"
    expected_output="$3"
    if [ "$SKIP_ERROR_CHECKS" == 1 ] && [ "$expected_status" != 0 ]
    then
        echo "[SKIP] $title"
        return
    fi

    shift
    shift
    shift
    output="$("$@" 2>&1)"
    exit_status=$?

    if [ "$exit_status" != "$expected_status" ] || [ "$output" != "$expected_output" ]
    then
        echo "[FAIL] $title"
        echo
        echo "$ $@"
        echo "Exit status: $exit_status"
        echo "Output:"
        echo "$output"
        echo
        echo "Expected exit status: $expected_status"
        echo "Expected output:"
        echo "$expected_output"
        echo
        echo "Diff:"
        diff <(echo "$expected_output") <(echo "$output")
        if [ "$EXIT_ON_FAILURE" == 1 ]
        then
            exit 1
        fi
    else
        echo "[OK] $title"
    fi
}

function write_config_file() {
    cat << EOF > "$HDD_REPLICAS_CONFIG_FILE"
    [main]
    database = sqlite:///tests/database.sqlite
    hash_algo = md5
    hash_print_len = 7
    replicas = 2
    check_days = 90
EOF
}
write_config_file
