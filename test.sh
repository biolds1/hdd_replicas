#!/bin/bash
. ./test_common.sh

GLOBAL_USAGE="usage: ./hddreplicas.py <command> [arguments ...]

with command:

     files    Files management
       hdd    Disks management
      help    Shows help
     mount    Mount points management
    status    Global status
    update    Disks data indexing, replication..."

MOUNT_USAGE="usage: ./hddreplicas.py mount <command> [arguments ...]

Mount points management

with command:

       add <path to mount point>                    Register a new location as mount point
        ls                                          List existing mount points
        rm <mount point id>                         Removes a mount point"

STATUS_USAGE="usage: ./hddreplicas.py status

Global status"

check "Usage on no command" 1 "$GLOBAL_USAGE" ./hddreplicas.py
check "Usage on bad command" 1 "$GLOBAL_USAGE" ./hddreplicas.py unknown
check "Usage on help command" 1 "$GLOBAL_USAGE" ./hddreplicas.py help
check "Usage on help subcommand" 1 "$MOUNT_USAGE" ./hddreplicas.py help mount
check "Usage on invalid subcommand" 1 "$MOUNT_USAGE" ./hddreplicas.py mount help
check "Usage on subcommand extra param" 1 "$STATUS_USAGE" ./hddreplicas.py status test
check "Usage on subcommand missing param" 1 "$MOUNT_USAGE" ./hddreplicas.py mount add
check "Usage on subcommand extra param" 1 "$MOUNT_USAGE" ./hddreplicas.py mount add location blah
check "Usage on missing subcommand params" 1 "$MOUNT_USAGE" ./hddreplicas.py mount
check "Usage on missing subcommand params" 1 "$MOUNT_USAGE" ./hddreplicas.py mount add

check "Add mount point wrong path" 1 'Failed: Path "tests/mount1" is not a directory.' ./hddreplicas.py mount add "$TEST_DIR/mount1"

mkdir "$TEST_DIR/mount1"
check "Add mount point" 0 "Mount point added." ./hddreplicas.py mount add "$TEST_DIR/mount1"

MOUNT_LS="Id  Path          State  Device
--  ------------  -----  ------
1   tests/mount1  Empty

Count: 1"

check "List mounts points" 0 "$MOUNT_LS" ./hddreplicas.py mount ls

DUPLICATE_MOUNT="(sqlite3.IntegrityError) UNIQUE constraint failed: mount.path
[SQL: INSERT INTO mount (path) VALUES (?)]
[parameters: ('tests/mount1',)]
(Background on this error at: https://sqlalche.me/e/20/gkpj)"

check "Add duplicate mount point" 1 "$DUPLICATE_MOUNT" ./hddreplicas.py mount add "$TEST_DIR/mount1"

mkdir "$TEST_DIR/mount2" "$TEST_DIR/mount3"
check "Add 2nd mount point" 0 "Mount point added." ./hddreplicas.py mount add "$TEST_DIR/mount2"
check "Add 3rd mount point" 0 "Mount point added." ./hddreplicas.py mount add "$TEST_DIR/mount3"

check "Remove 3rd mount point" 0 'Mount point "tests/mount3" (3) removed.' ./hddreplicas.py mount rm 3

MOUNT_LS="Id  Path          State  Device
--  ------------  -----  ------
1   tests/mount1  Empty
2   tests/mount2  Empty

Count: 2"

check "List mounts points" 0 "$MOUNT_LS" ./hddreplicas.py mount ls

check "Remove non-existing mount point" 1 'No such mount point exists (id 5).' ./hddreplicas.py mount rm 5
check "Remove invalid mount point" 1 'No such mount point exists (id blah).' ./hddreplicas.py mount rm blah
check "List mount points" 0 "$MOUNT_LS" ./hddreplicas.py mount ls

check "Add a HDD1" 0 "HDD added." ./hddreplicas.py hdd add 1 HDD1

HDD_CONF_CONTENT="[main]
name = HDD1
"
check "Check HDD1 conf file" 0 "" diff <(echo "$HDD_CONF_CONTENT") "$TEST_DIR/mount1/.hdd_replicas.conf"

HDD_LS="Id  Name  State                        To verify  Total size  Available  Used   Free
--  ----  ---------------------------  ---------  ----------  ---------  -----  ----
1   HDD1  Mounted on tests/mount1 (1)  0          1.0 kB      1.0 kB     0.0 B  100%

Count: 1"

check "List HDDs" 0 "$HDD_LS" ./hddreplicas.py hdd ls

check "Add already registered HDD1" 1 'Failed: Disk mounted under "tests/mount1" is already registered as disk "HDD1" (file tests/mount1/.hdd_replicas.conf exists).' ./hddreplicas.py hdd add 1 HDD-bad
check "Add an HDD with existing name" 1 "Failed: an HDD with this name is already registered (1)." ./hddreplicas.py hdd add 2 HDD1
check "Add an HDD on inexisting mount point" 1 "No such mount point exists (id 999)." ./hddreplicas.py hdd add 999 HDD1
check "Add an HDD on invalid mount point" 1 "No such mount point exists (id azerty)." ./hddreplicas.py hdd add azerty HDD1

check "No config file was created for existing HDD" 1 "" test -e "$TEST_DIR/mount2/.hdd_replicas.conf"
check "Add a HDD2" 0 "HDD added." ./hddreplicas.py hdd add 2 HDD2
check "Config file was created for HDD2" 0 "" test -e "$TEST_DIR/mount2/.hdd_replicas.conf"

HDD_LS="Id  Name  State                        To verify  Total size  Available  Used   Free
--  ----  ---------------------------  ---------  ----------  ---------  -----  ----
1   HDD1  Mounted on tests/mount1 (1)  0          1.0 kB      1.0 kB     0.0 B  100%
2   HDD2  Mounted on tests/mount2 (2)  0          1.0 kB      1.0 kB     0.0 B  100%

Count: 2"

check "List HDDs" 0 "$HDD_LS" ./hddreplicas.py hdd ls

./hddreplicas.py mount add "$TEST_DIR/mount3" > /dev/null
MOUNT3_ID=$(./hddreplicas.py mount ls | grep mount3 | awk '{print $1}')
check "Add a HDD3" 0 "HDD added." ./hddreplicas.py hdd add "$MOUNT3_ID" HDD3
./hddreplicas.py mount rm "$MOUNT3_ID" > /dev/null

check "Remove HDD3" 0 'HDD "HDD3" (3) removed.' ./hddreplicas.py hdd rm 3
check "List HDDs" 0 "$HDD_LS" ./hddreplicas.py hdd ls

MOUNT_LS_MOUNTED='Id  Path          State               Device
--  ------------  ------------------  ------
1   tests/mount1  HDD "HDD1" mounted
2   tests/mount2  HDD "HDD2" mounted

Count: 2'

check "Mounted mount points" 0 "$MOUNT_LS_MOUNTED" ./hddreplicas.py mount ls

tar cf "$TEST_DIR/mounts-empty.tar" "$TEST_DIR/mount1" "$TEST_DIR/mount2" "$TEST_DIR"/database.sqlite
rm "$TEST_DIR"/mount*/.*

HDD_LS_UNMOUNTED="Id  Name  State    To verify  Total size  Available  Used   Free
--  ----  -------  ---------  ----------  ---------  -----  ----
1   HDD1  Offline  0          1.0 kB      1.0 kB     0.0 B  100%
2   HDD2  Offline  0          1.0 kB      1.0 kB     0.0 B  100%

Count: 2"

check "Unmounted HDD list" 0 "$HDD_LS_UNMOUNTED" ./hddreplicas.py hdd ls

tar xf "$TEST_DIR/mounts-empty.tar" "$TEST_DIR/mount1"
echo 'file1' > "$TEST_DIR/mount1/file1"

UPDATE_NEW_FILE_INDEX="Updating files state of HDD1...
Hashing file file1
Handling stale metadata...
Replicating files...

1 files are missing replicas, some replicas could be done if the following disks are mounted together:

Disks          Pending replicas
-------------  ----------------
HDD1 and HDD2  1"
check "New file indexing" 1 "$UPDATE_NEW_FILE_INDEX" ./hddreplicas.py update

FILE_SEARCH_MATCH="Id  Filename  HDD   Size   Md5
--  --------  ----  -----  -------
1   file1     HDD1  6.0 B  5149d40

Count: 1"
check "Search files insensitive" 0 "$FILE_SEARCH_MATCH" ./hddreplicas.py files search 'FILE1'
check "Search files insensitive exact" 0 "$FILE_SEARCH_MATCH" ./hddreplicas.py files search '^file1$'
check "Search files sensitive match" 0 "$FILE_SEARCH_MATCH" ./hddreplicas.py files ssearch '^file1$'

FILE_SEARCH_NO_MATCH="Id  Filename  HDD  Size  Md5
--  --------  ---  ----  ---

Count: 0"
check "Search files no match insensitive" 0 "$FILE_SEARCH_NO_MATCH" ./hddreplicas.py files search 'blah'
check "Search files no match sensitive" 0 "$FILE_SEARCH_NO_MATCH" ./hddreplicas.py files ssearch '^File1$'

tar xf "$TEST_DIR/mounts-empty.tar" "$TEST_DIR/mount2"

UPDATE_NEW_REPLICA_INDEX="Updating files state of HDD1...
Updating files state of HDD2...
Handling stale metadata...
Replicating files...
Copying file1 from \"HDD1\" to \"HDD2\""
check "File replication" 0 "$UPDATE_NEW_REPLICA_INDEX" ./hddreplicas.py update

FILE_SEARCH_REPLICATED="Id  Filename  HDD   Size   Md5
--  --------  ----  -----  -------
1   file1     HDD1  6.0 B  5149d40
2   file1     HDD2  6.0 B  5149d40

Count: 2"
check "Search replicated files" 0 "$FILE_SEARCH_REPLICATED" ./hddreplicas.py files search 'file1'

UPDATE_NOTHING_TO_DO="Updating files state of HDD1...
Updating files state of HDD2...
Handling stale metadata...
Replicating files..."
check "File replicated, nothing to do" 0 "$UPDATE_NOTHING_TO_DO" ./hddreplicas.py update

mkdir "$TEST_DIR/mount2/sub"
echo file2 > "$TEST_DIR/mount2/sub/file2"

UPDATE_SUBDIR_REPLICATE="Updating files state of HDD1...
Updating files state of HDD2...
Hashing file sub/file2
Handling stale metadata...
Replicating files...
Copying sub/file2 from \"HDD2\" to \"HDD1\""
check "subdir file replication" 0 "$UPDATE_SUBDIR_REPLICATE" ./hddreplicas.py update

FILE_SEARCH_SUBDIR="Id  Filename   HDD   Size   Md5
--  ---------  ----  -----  -------
4   sub/file2  HDD1  6.0 B  3d709e8
3   sub/file2  HDD2  6.0 B  3d709e8

Count: 2"
check "subdir file search" 0 "$FILE_SEARCH_SUBDIR" ./hddreplicas.py files search sub/

tar cf "$TEST_DIR/all-files.tar" "$TEST_DIR"/{config,database*,mount{1..3}}

rm "$TEST_DIR/mount1/sub/file2"

UPDATE_REPLICATED_FILE_DELETE="Updating files state of HDD1...
Removing empty directory sub from HDD1
Updating files state of HDD2...
Handling stale metadata...
Deleting sub/file2 from HDD2
Removing empty directory sub from HDD2
Replicating files..."

check "replicated file delete" 0 "$UPDATE_REPLICATED_FILE_DELETE" ./hddreplicas.py update

function check_replicated_file_delete() {
    check "replicated file was deleted" 0 "" test ! -e "$TEST_DIR/mount2/sub/file2"
    check "replicated parent dir was deleted" 0 "" test ! -e "$TEST_DIR/mount2/sub"
    check "replicated original file was deleted" 0 "" test ! -e "$TEST_DIR/mount1/sub/file2"
    check "replicated original parent dir was deleted" 0 "" test ! -e "$TEST_DIR/mount1/sub"
    check "search remaining files after delete" 0 "$FILE_SEARCH_REPLICATED" ./hddreplicas.py files search .
    check "replicated file delete, src db entry deleted" 1 "No such file exists (id 3)." ./hddreplicas.py debug show_file 3
    check "replicated file delete, replica db entry deleted" 1 "No such file exists (id 4)." ./hddreplicas.py debug show_file 4
}
check_replicated_file_delete

rm -rf "$TEST_DIR"/mount{1..3}/
tar xf "$TEST_DIR/all-files.tar"
mv "$TEST_DIR/mount1/sub/file2" "$TEST_DIR/mount1/sub/renamed"

UPDATE_REPLICATED_FILE_RENAMED="Updating files state of HDD1...
Hashing file sub/renamed
Updating files state of HDD2...
Handling stale metadata...
Renaming sub/file2 to sub/renamed on HDD2
Replicating files..."

check "replicated file rename" 0 "$UPDATE_REPLICATED_FILE_RENAMED" ./hddreplicas.py update

function check_replicated_file_renamed {
    check "replicated file was renamed" 0 "" test ! -e "$TEST_DIR/mount2/sub/file2"
    check "replicated origin file was deleted" 0 "" test ! -e "$TEST_DIR/mount2/sub/file2"
    check "replicated target file was copied" 0 "" bash -c 'test "$(md5sum < "'$TEST_DIR'/mount1/sub/renamed")" = "$(md5sum < "'$TEST_DIR'/mount2/sub/renamed")"'

    FILE_SEARCH_RENAMED="Id  Filename     HDD   Size   Md5
--  -----------  ----  -----  -------
1   file1        HDD1  6.0 B  5149d40
2   file1        HDD2  6.0 B  5149d40
5   sub/renamed  HDD1  6.0 B  3d709e8
3   sub/renamed  HDD2  6.0 B  3d709e8

Count: 4"

    check "search remaining files after rename" 0 "$FILE_SEARCH_RENAMED" ./hddreplicas.py files search .
    check "replicated file rename, src db entry deleted" 1 "No such file exists (id 4)." ./hddreplicas.py debug show_file 4
}
check_replicated_file_renamed

rm -rf "$TEST_DIR"/mount{1..3}/ "$TEST_DIR"/database.sqlite
tar xf "$TEST_DIR"/mounts-empty.tar
mkdir -p "$TEST_DIR"/mount{1,2}/sub/sdf/
echo file1 > "$TEST_DIR"/mount1/sub/sdf/file1
echo file1 > "$TEST_DIR"/mount2/sub/sdf/file1

SUBDIR_RENAME="Updating files state of HDD1...
Hashing file sub/sdf/file1
Updating files state of HDD2...
Hashing file sub/sdf/file1
Handling stale metadata...
Replicating files..."
check "file rename to new subdir indexing" 0 "$SUBDIR_RENAME" ./hddreplicas.py update

mkdir "$TEST_DIR"/mount1/blah/
mv "$TEST_DIR"/mount1/sub/sdf/file1 "$TEST_DIR"/mount1/blah/renamed

SUBDIR_RENAME_UPDATE="Updating files state of HDD1...
Hashing file blah/renamed
Removing empty directory sub/sdf from HDD1
Removing empty directory sub from HDD1
Updating files state of HDD2...
Handling stale metadata...
Renaming sub/sdf/file1 to blah/renamed on HDD2
Removing empty directory sub/sdf from HDD2
Removing empty directory sub from HDD2
Replicating files..."

check "file rename to new subdir" 0 "$SUBDIR_RENAME_UPDATE" ./hddreplicas.py update

rm -rf "$TEST_DIR"/mount{1..3}/
tar xf "$TEST_DIR"/all-files.tar
rm "$TEST_DIR"/mount2/.hdd_replicas.conf
rm "$TEST_DIR"/mount1/sub/file2
DELETED_FILE_ID="$(./hddreplicas.py files search sub/file2 | grep HDD1 | awk '{print $1}')"

UPDATE_REPLICATED_FILE_DELETE_OFFLINE="Updating files state of HDD1...
Removing empty directory sub from HDD1
Handling stale metadata...
Replicating files..."

check "replicated file delete, replica offline" 0 "$UPDATE_REPLICATED_FILE_DELETE_OFFLINE" ./hddreplicas.py update

DELETED_FILE="File 4
- -
Id 4
Name sub/file2
Filesize 6.0 B
Hdd.Name HDD1
Created False
Deleted True
Hash 3d709e89c8ce201e3c928eb917989aef"

check "replicated file delete, replica offline, deleted file ref kept" 0 "$DELETED_FILE" bash -c './hddreplicas.py debug show_file "'$DELETED_FILE_ID'" | grep -v ^Last_ | sed -e "s/ \+/ /g" -e "s/-\+/-/g"'

FILE_SEARCH_HIDES_DELETED="Id  Filename   HDD   Size   Md5
--  ---------  ----  -----  -------
1   file1      HDD1  6.0 B  5149d40
2   file1      HDD2  6.0 B  5149d40
3   sub/file2  HDD2  6.0 B  3d709e8

Count: 3"

check "replicated file delete, replica offline, file list" 0 "$FILE_SEARCH_HIDES_DELETED" ./hddreplicas.py files search .

tar xf "$TEST_DIR"/all-files.tar "$TEST_DIR"/mount2/.hdd_replicas.conf

UPDATE_REPLICATED_FILE_DELETED_REPLICA_BACK_ONLINE="Updating files state of HDD1...
Updating files state of HDD2...
Handling stale metadata...
Deleting sub/file2 from HDD2
Removing empty directory sub from HDD2
Replicating files..."
check "replicated file delete, replica back online" 0 "$UPDATE_REPLICATED_FILE_DELETED_REPLICA_BACK_ONLINE" ./hddreplicas.py update
check_replicated_file_delete

rm -rf "$TEST_DIR"/mount{1..3}/
tar xf "$TEST_DIR"/all-files.tar
rm "$TEST_DIR"/mount2/.hdd_replicas.conf
mv "$TEST_DIR"/mount1/sub/file2 "$TEST_DIR"/mount1/sub/renamed
RENAMED_FILE_ID="$(./hddreplicas.py files search sub/file2 | grep HDD1 | awk '{print $1}')"

UPDATE_REPLICATED_FILE_RENAME_REPLICA_OFFLINE="Updating files state of HDD1...
Hashing file sub/renamed
Handling stale metadata...
Replicating files...

1 files are missing replicas, some replicas could be done if the following disks are mounted together:

Disks          Pending replicas
-------------  ----------------
HDD1 and HDD2  1"

check "replicated file rename, replica offline" 1 "$UPDATE_REPLICATED_FILE_RENAME_REPLICA_OFFLINE" ./hddreplicas.py update

RENAMED_FILE="File 4
- -
Id 4
Name sub/file2
Filesize 6.0 B
Hdd.Name HDD1
Created False
Deleted True
Hash 3d709e89c8ce201e3c928eb917989aef"

check "replicated file rename, deleted file ref kept" 0 "$RENAMED_FILE" bash -c './hddreplicas.py debug show_file "'$RENAMED_FILE_ID'" | grep -v ^Last_ | sed -e "s/ \+/ /g" -e "s/-\+/-/g"'

FILE_SEARCH_HIDES_RENAMED="Id  Filename     HDD   Size   Md5
--  -----------  ----  -----  -------
1   file1        HDD1  6.0 B  5149d40
2   file1        HDD2  6.0 B  5149d40
3   sub/file2    HDD2  6.0 B  3d709e8
5   sub/renamed  HDD1  6.0 B  3d709e8

Count: 4"

check "replicated file rename, replica offline, file list" 0 "$FILE_SEARCH_HIDES_RENAMED" ./hddreplicas.py files search .

tar xf "$TEST_DIR"/all-files.tar "$TEST_DIR"/mount2/.hdd_replicas.conf

UPDATE_REPLICATED_FILE_RENAME_REPLICA_BACK_ONLINE="Updating files state of HDD1...
Updating files state of HDD2...
Handling stale metadata...
Renaming sub/file2 to sub/renamed on HDD2
Replicating files..."

check "replicated file rename, replica back online" 0 "$UPDATE_REPLICATED_FILE_RENAME_REPLICA_BACK_ONLINE" ./hddreplicas.py update
check_replicated_file_renamed

mkdir -p "$TEST_DIR"/mount2/tmp/
touch "$TEST_DIR"/mount2/tmp/.hdd_replicas_copy.tmp

STALE_TMP_FILE_RM="Updating files state of HDD1...
Updating files state of HDD2...
Removing empty directory tmp from HDD2
Handling stale metadata...
Replicating files..."

check "stale temporary file delete" 0 "$STALE_TMP_FILE_RM" ./hddreplicas.py update
check "stale temporary file was deleted" 0 "" test ! -e mount2/tmp/.hdd_replicas_copy.tmp
check "stale temporary file parent dir was deleted" 0 "" test ! -e mount2/tmp

rm -rf "$TEST_DIR"/mount{1..3}/ "$TEST_DIR"/database.sqlite
tar xf "$TEST_DIR"/mounts-empty.tar
rm "$TEST_DIR"/mount2/.hdd_replicas.conf

echo file1 > "$TEST_DIR"/mount1/file1
check "file corruption initial hashing" 1 "$UPDATE_NEW_FILE_INDEX" ./hddreplicas.py update

tar xf "$TEST_DIR"/mounts-empty.tar "$TEST_DIR"/mount2/.hdd_replicas.conf
echo aaaa > "$TEST_DIR"/mount1/file1

CORRUPTED_FILE="Updating files state of HDD1...
Updating files state of HDD2...
Handling stale metadata...
Replicating files...
Copying file1 from \"HDD1\" to \"HDD2\"
Failed: The hash of \"file1\" (1) on HDD1 has changed since it was first indexed"

check "file corruption detected on copy" 1 "$CORRUPTED_FILE" ./hddreplicas.py update


rm -rf "$TEST_DIR"/mount{1..3}/ "$TEST_DIR"/database.sqlite
tar xf "$TEST_DIR"/mounts-empty.tar
echo aaa > "$TEST_DIR"/mount1/file1
echo aaa > "$TEST_DIR"/mount1/file2
echo bbb > "$TEST_DIR"/mount1/file3

DUPLICATE_INDEXING="Updating files state of HDD1...
Hashing file file1
Hashing file file2
Hashing file file3
Updating files state of HDD2...
Handling stale metadata...
Replicating files...
Copying file1 from \"HDD1\" to \"HDD2\"
Copying file2 from \"HDD1\" to \"HDD2\"
Copying file3 from \"HDD1\" to \"HDD2\""

check "duplicate hash indexing" 0 "$DUPLICATE_INDEXING" ./hddreplicas.py update

FILES_DUPLICATES="Filename  Md5      HDD
--------  -------  ----
file1     5c9597f  HDD1
file1     5c9597f  HDD2
file2     5c9597f  HDD1
file2     5c9597f  HDD2

Count: 4"

check "duplicates finding" 0 "$FILES_DUPLICATES" ./hddreplicas.py files duplicates

rm "$TEST_DIR"/mount1/file2

DUPLICATE_FILE_DELETE_UPDATE="Updating files state of HDD1...
Updating files state of HDD2...
Handling stale metadata...
Deleting file2 from HDD2
Replicating files..."
check "duplicates after delete update" 0 "$DUPLICATE_FILE_DELETE_UPDATE" ./hddreplicas.py update

rm -rf "$TEST_DIR"/mount{1..3}/ "$TEST_DIR"/database.sqlite
tar xf "$TEST_DIR"/mounts-empty.tar
echo file1 > "$TEST_DIR"/mount1/file1
echo file1 > "$TEST_DIR"/mount2/file1

FILES_HASH_INIT="Updating files state of HDD1...
Hashing file file1
Updating files state of HDD2...
Hashing file file1
Handling stale metadata...
Replicating files..."

check "files hash check init" 0 "$FILES_HASH_INIT" ./hddreplicas.py update
check "files hash check nothing to do" 0 "0 files checked." ./hddreplicas.py files check

sed -e 's/check_days .*/check_days = 0/' -i "$HDD_REPLICAS_CONFIG_FILE"

HASH_CHECK_NOTIFICATION="Updating files state of HDD1...
Updating files state of HDD2...
Handling stale metadata...
Replicating files...

** The integrity of 2 files has not been checked since more than 0 days **.
** Mount drive \"HDD1\", and run \"./hddreplicas.py files check\" to update them. **"

check "files hash check notification" 0 "$HASH_CHECK_NOTIFICATION" ./hddreplicas.py update

HASH_CHECK_SUCCESS="Hashing file1 on HDD1
Hashing file1 on HDD2
2 files checked."
check "files hash check success" 0 "$HASH_CHECK_SUCCESS" ./hddreplicas.py files check

echo aaa > "$TEST_DIR"/mount1/file1
HASH_CHECK_FAILED="Hashing file1 on HDD1
WARNING: hash of file (5c9597f3c8245907ea71a89d9d39d08e) has changed (was 5149d403009a139c7e085405ef762e1a)
Hashing file1 on HDD2
2 files checked."
check "files hash check failure" 1 "$HASH_CHECK_FAILED" ./hddreplicas.py files check

CONFLICT_FILE_ID="$(./hddreplicas.py files search file1 | grep HDD1 | awk '{print $1}')"
REHASH_FILE1='Rehashing "file1" on "HDD1"
Done'
check "file rehash" 0 "$REHASH_FILE1" ./hddreplicas.py files rehash "$CONFLICT_FILE_ID"

echo aaa > "$TEST_DIR"/mount1/file1
REHASH_FILE2='Rehashing "file1" on "HDD2"
Done'
CONFLICT_FILE_ID="$(./hddreplicas.py files search file1 | grep HDD2 | awk '{print $1}')"
check "file replica rehash" 0 "$REHASH_FILE2" ./hddreplicas.py files rehash "$CONFLICT_FILE_ID"

write_config_file
check "file rehash check" 0 "$UPDATE_NOTHING_TO_DO" ./hddreplicas.py update
