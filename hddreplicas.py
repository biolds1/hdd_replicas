#!venv/bin/python3
import inspect
import hashlib
import os
import sys
from argparse import ArgumentParser
from collections import namedtuple, OrderedDict
from configparser import ConfigParser
from copy import copy
from datetime import date, datetime, timedelta
from shutil import disk_usage
from typing import List
from typing import Optional

from sqlalchemy import create_engine, delete, func, update, select, BigInteger, Boolean, Date, DateTime, ForeignKey, Text, UniqueConstraint
from sqlalchemy.exc import DatabaseError
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship, Session

HDD_CONF_FILENAME = '.hdd_replicas.conf'
HDD_TMP_FILENAME = '.hdd_replicas_copy.tmp'
SQL_BATCH_SIZE = 1024


if os.getenv('HDD_REPLICAS_UNDER_TEST'):
    def disk_usage(p):
        DU = namedtuple('DU', ['total', 'used', 'free'])
        if 'full' in p:
            return DU(total=1000, used=1000, free=0)
        return DU(total=1000, used=0, free=1000)


def get_unit(n):
    units = ['', 'k', 'M', 'G', 'T', 'P']
    unit_no = 0
    while n >= 1000:
        unit_no += 1
        n /= 1000
    return 10 ** (unit_no * 3), units[unit_no]


def human_count(n, f='%0.1f'):
    factor, unit = get_unit(n)
    fmt = '%s %%s' % f
    return fmt % (n / factor, unit)


def human_filesize(n):
    return human_count(n) + 'B'


def recursive_rmdir(hdd, name, d):
    while os.listdir(d) == []:
        print('Removing empty directory', name, 'from', hdd)
        os.rmdir(d)
        d = os.path.dirname(d)
        name = os.path.dirname(name)

class Base(DeclarativeBase):
    pass


class DBMount(Base):
    __tablename__ = 'mount'

    id: Mapped[int] = mapped_column(primary_key=True)
    path: Mapped[str] = mapped_column(Text)
    __table_args__ = (UniqueConstraint('path', name='mount_unique_path'),)


class DBHDD(Base):
    __tablename__ = 'hdd'

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(Text)
    files: Mapped[List['DBFile']] = relationship(back_populates='hdd')
    available: Mapped[int] = mapped_column(BigInteger)
    size: Mapped[int] = mapped_column(BigInteger)
    __table_args__ = (UniqueConstraint('name', name='hdd_unique_name'),)

    def update_size(self, mount_path):
        stat = disk_usage(mount_path)
        self.size = stat.total
        self.available = stat.free


class DBFile(Base):
    __tablename__ = 'file'

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(Text)
    hash: Mapped[str] = mapped_column(Text)
    size: Mapped[int] = mapped_column(BigInteger)
    last_seen: Mapped[datetime] = mapped_column(DateTime)
    last_checked: Mapped[date] = mapped_column(Date)
    created: Mapped[bool] = mapped_column(Boolean, default=True)
    deleted: Mapped[bool] = mapped_column(Boolean, default=False)
    hdd_id: Mapped[int] = mapped_column(ForeignKey('hdd.id'))
    hdd: Mapped[DBHDD] = relationship(back_populates='files')
    __table_args__ = (UniqueConstraint('name', 'hdd_id', name='file_unicity_constraint'),)


class ProgressBar:
    def __init__(self, _max):
        self.displayed = False
        self._max = max(_max, 1)
        self.columns = 20
        self.start = datetime.now()
        self.last = None

    def clear(self):
        if self._max < 1024 * 1024:
            return
        print('\33[2K\33[0G', end='')

    def show(self, val, prefix='', suffix=''):
        if self._max < 1024 * 1024:
            return
        now = datetime.now()
        if self.last is None or self.last + timedelta(seconds=1) < now:
            self.last = now

            if self.displayed:
                self.clear()

            self.displayed = True

            done = int(self.columns * val / self._max)
            remaining = self.columns - done
            if done > 1:
                progress = (done - 1)  * '='
                progress += '>'
            else:
                progress = done * '='

            if prefix:
                prefix += ' '
            if suffix:
                suffix = ' ' + suffix

            speed = val / (now - self.start).total_seconds()
            dt = str(timedelta(seconds=(self._max - val) / speed))
            if '.' in dt:
                dt, _ = dt.split('.', 1)

            sys.stdout.write('%s%s/s %s [%s%s] %0.1i%%%s' % (prefix, human_filesize(speed), dt, progress, ' ' * remaining, 100 * val / self._max, suffix))
            sys.stdout.flush()


class Command:
    _subcommands = {}

    def __init__(self, db, cfg):
        self.db = db
        self.session = Session(db)
        self.cfg = cfg
        self.run_date = datetime.now()

    @classmethod
    def class_map(cls):
        command_str_map = {}
        for cls in copy(globals()).values():
            try:
                if issubclass(cls, Command) and cls != Command:
                    command_str_map[cls._id] = cls
            except TypeError:
                pass
        return command_str_map

    @classmethod
    def get_class(cls, name):
        return cls.class_map()[name]

    @classmethod
    def usage(cls):
        print(f'''usage: {sys.argv[0]} <command> [arguments ...]

with command:
''')
        for name in sorted(cls.class_map().keys()):
            if name == 'debug':
                continue
            cmd = cls.get_class(name)
            print('%10s    %s' % (cmd._id, cmd._help))
        exit(1)

    @classmethod
    def handle(cls, engine, cfg):
        if len(sys.argv) < 2:
            cls.usage()

        command = sys.argv[1]
        try:
            cmd_cls = cls.get_class(command)
        except KeyError:
            cls.usage()

        try:
            cmd_cls(engine, cfg)._run()
        except DatabaseError as e:
            print(e)
            exit(1)

    @classmethod
    def subusage(cls):
        sub = sorted(cls._subcommands.keys())
        params = ''
        cmd = ''
        if sub:
            cmd = ' <command> [arguments ...]'
            params = '\n\nwith command:\n'
        print(f'''usage: {sys.argv[0]} {cls._id}{cmd}

{cls._help}{params}''')

        for name in sub:
            print('%10s %s %s' % (name, cls._subcommands[name].get('params', '').ljust(40),
                                  cls._subcommands[name]['help']))
        exit(1)

    def _run(self):
        method = getattr(self, 'run', None)
        if method is not None:
            params = sys.argv[2:]

            if len(params) + 1 != len(inspect.getfullargspec(method)[0]):
                self.subusage()
            return self.run(*params)


        if len(sys.argv) < 3:
            self.subusage()

        subcommand = sys.argv[2]
        method = getattr(self, 'run_%s' % subcommand, None)
        if method is None:
            self.subusage()

        params = sys.argv[3:]

        if len(params) + 1 != len(inspect.getfullargspec(method)[0]):
            self.subusage()

        method(*params)

    @classmethod
    def _field_resolve(cls, obj, p):
        if isinstance(p, int):
            return obj[p]

        for attr in p.split('.'):
            if isinstance(obj, (dict, list)):
                obj = obj[attr]
            else:
                obj = getattr(obj, attr)
        return obj

    @classmethod
    def show_data(cls, data, titles):
        col_lens = []
        for attr, title in titles.items():
            col_lens.append(len(title))
            for line in data:
                col_lens[-1] = max(len(str(cls._field_resolve(line, attr))), col_lens[-1])

        s = ''
        dashes = ''
        for i, title in enumerate(titles.values()):
            dashes += col_lens[i] * '-'
            if i + 1 < len(titles):
                s += title.ljust(col_lens[i])
                s += '  '
                dashes += '  '
            else:
                s += title
        print(s.rstrip())
        print(dashes)

        for line in data:
            s = ''
            for i, col in enumerate(titles.keys()):
                val = str(cls._field_resolve(line, col))
                if i + 1 < len(titles):
                    s += val.ljust(col_lens[i])
                    s += '  '
                else:
                    s += val
            print(s.rstrip())

    @classmethod
    def show_tables(cls, data, titles):
        cls.show_data(data, titles)
        print()
        print('Count: %s' % len(data))

    @classmethod
    def show_obj(cls, obj_name, obj, attrs):
        titles = OrderedDict((('key', obj_name), ('value', '')))
        data = []
        for attr in attrs:
            data.append({
                'key': attr.title(),
                'value': cls._field_resolve(obj, attr)
            })
        cls.show_data(data, titles)

    @classmethod
    def obj_by_id(cls, session, _id, attr='id', fail_on_missing=True):
        lookup = getattr(cls._table, attr)
        query = select(cls._table).where(lookup == _id)
        obj = session.scalars(query).first()
        if fail_on_missing and obj is None:
            print('No such', cls._name, 'exists (%s %s).' % (attr, _id))
            exit(1)
        return obj


class HelpCommand(Command):
    _id = 'help'
    _help = 'Shows help'

    def _run(self):
        if len(sys.argv) > 2:
            try:
                cls = self.get_class(sys.argv[2])
                cls.subusage()
            except KeyError:
                self.usage()
        self.usage()


class StatusCommand(Command):
    _id = 'status'
    _help = 'Global status'

    def run(self):
        total_space = self.session.query(func.sum(DBHDD.size)).one()[0]
        free_space = self.session.query(func.sum(DBHDD.available)).one()[0]
        files_count = self.session.query(func.count(DBFile.name)).one()[0]
        unique_files = self.session.query(DBFile.name).distinct().count()
        oldest_check = self.session.scalars(select(DBFile.last_checked).order_by(DBFile.last_checked.asc()).limit(1)).first()
        files_to_check = FilesCommand.files_to_check(self.run_date.date(), self.session, self.cfg, True)
        to_check_count = files_to_check.count()
        replicas_count = int(self.cfg['main']['replicas'])

        data = OrderedDict((
            ('Total space', human_filesize(total_space)),
            ('Free space', human_filesize(free_space)),
            ('Total replicated', human_filesize(total_space / replicas_count)),
            ('Free space replicated', human_filesize(free_space / replicas_count)),
            ('Files count', human_count(files_count)),
            ('Unique files', human_count(unique_files)),
            ('Next check', oldest_check + timedelta(days=int(cfg['main']['check_days']))),
            ('Files to check now', human_count(to_check_count, '%i'))
        ))
        self.show_obj('Status', data, data.keys())

        FilesCommand.files_to_check_warn(self.run_date.date(), self.session, self.cfg)


class Disk:
    def __init__(self, session, mount_path):
        self.session = session
        self.mount_path = mount_path
        self.hdd_desc_file = os.path.join(mount_path, HDD_CONF_FILENAME)
        self.cfg = ConfigParser()
        self.cfg['main'] = {}
        self._db_obj = None

    def read(self):
        with open(self.hdd_desc_file) as fd:
            self.cfg.read_file(fd)

    def write(self):
        with open(self.hdd_desc_file, 'w') as fd:
            self.cfg.write(fd)

    @classmethod
    def load(cls, session, mount_path):
        disk = Disk(session, mount_path)
        if not os.path.exists(disk.hdd_desc_file):
            return None
        disk.read()
        if disk.db_obj(fail_on_missing=False) is None:
            return None
        return disk

    @property
    def name(self):
        return self.cfg['main']['name'] if self.cfg else ''

    @name.setter
    def name(self, name):
        self.cfg['main']['name'] = name

    def db_obj(self, fail_on_missing=True):
        if not self._db_obj:
            self._db_obj = HDDCommand.obj_by_id(self.session, self.name, 'name', fail_on_missing)
            if self._db_obj is None:
                return
        self.update_size()
        self.session.add_all([self._db_obj])
        self.session.commit()
        return self._db_obj

    def update_size(self):
        self._db_obj.update_size(self.mount_path)


class MountCommand(Command):
    _id = 'mount'
    _help = 'Mount points management'
    _name = 'mount point'
    _subcommands = {
        'ls': {
            'help': 'List existing mount points',
        },
        'add': {
            'params': '<path to mount point>',
            'help': 'Register a new location as mount point'
        },
        'rm': {
            'params': '<mount point id>',
            'help': 'Removes a mount point'
        },
    }
    _table = DBMount

    def run_ls(self):
        query = select(DBMount).order_by(DBMount.path.asc())
        mounts = list(self.session.scalars(query))

        has_devices = False
        devices_mounts = {}
        try:
            with open('/proc/mounts', 'r') as fd:
                for l in fd.readlines():
                    device, mount_dir, _ = l.split(' ', 2)
                    devices_mounts[mount_dir] = device
            has_devices = True
        except:
            pass

        for mount in mounts:
            disk = Disk.load(self.session, mount.path)
            if disk:
                state = 'HDD "%s" mounted' % disk.name
            elif os.listdir(mount.path):
                state = 'Unknown data available (use "hdd add" command to index it)'
            else:
                state = 'Empty'
            mount.state = state
            mount.device = devices_mounts.get(mount.path, '')

        cols = OrderedDict((('id', 'Id'), ('path', 'Path'), ('state', 'State')))
        if has_devices:
            cols['device'] = 'Device'
        self.show_tables(mounts, cols)

    def run_rm(self, mount_id):
        mount = self.obj_by_id(self.session, mount_id)
        self.session.delete(mount)
        self.session.commit()
        print('Mount point "%s" (%s) removed.' % (mount.path, mount.id))

    def run_add(self, mount_path):
        mount_path = mount_path.rstrip('/')
        if not os.getenv('HDD_REPLICAS_UNDER_TEST'):
            mount_path = os.path.abspath(mount_path)

        if not os.path.isdir(mount_path):
            print('Failed: Path "%s" is not a directory.' % mount_path)
            exit(1)
        mount = DBMount(path=mount_path)
        self.session.add_all([mount])
        self.session.commit()
        print('Mount point added.')


class HDDCommand(Command):
    _id = 'hdd'
    _help = 'Disks management'
    _name = 'disk'
    _subcommands = {
        'ls': {
            'help': 'List existing disks',
        },
        'add': {
            'params': '<mount point id> <hdd name>',
            'help': 'Register a new disk currently mounted under mount point'
        },
        'rm': {
            'params': '<disk id>',
            'help': 'Removes a disk'
        },
    }
    _table = DBHDD

    def run_ls(self):
        query = select(DBMount)
        mounts = list(self.session.scalars(query))
        mountpoints = {}
        for mount in mounts:
            disk = Disk.load(self.session, mount.path)
            if disk:
                mountpoints[disk.name] = mount

        query = select(DBHDD).order_by(DBHDD.name.asc())
        hdds = list(self.session.scalars(query))
        hdd_to_update = []

        last_check = self.run_date - timedelta(days=int(cfg['main']['check_days']))
        for hdd in hdds:
            mount = mountpoints.get(hdd.name, None)
            if mount:
                state = 'Mounted on %s (%s)' % (mount.path, mount.id)
                hdd.update_size(mount.path)
                hdd_to_update.append(hdd)
            else:
                state = 'Offline'
            hdd.state = state
            hdd.size_h = human_filesize(hdd.size)
            hdd.avail_h = human_filesize(hdd.available)
            hdd.used_h = human_filesize(hdd.size - hdd.available)
            hdd.free_pct = '%0.1i%%' % (hdd.available / hdd.size * 100)

            query = self.session.query(DBFile).where(DBFile.hdd_id == hdd.id) \
                    .where(DBFile.last_checked <= last_check)
            hdd.files_to_verify = query.count()

        self.session.add_all(hdd_to_update)
        self.session.commit()

        self.show_tables(hdds, OrderedDict((('id', 'Id'), ('name', 'Name'), ('state', 'State'), ('files_to_verify', 'To verify'), ('size_h', 'Total size'), ('avail_h', 'Available'), ('used_h', 'Used'), ('free_pct', 'Free'))))

    def run_rm(self, hdd_id):
        query = delete(DBFile).where(DBFile.hdd_id == int(hdd_id))
        self.session.execute(query)
        self.session.commit()
        hdd = self.obj_by_id(self.session, hdd_id)
        self.session.delete(hdd)
        self.session.commit()
        print('HDD "%s" (%s) removed.' % (hdd.name, hdd.id))

    def run_add(self, mount_id, name):
        mount = MountCommand.obj_by_id(self.session, mount_id)

        disk = Disk.load(self.session, mount.path)
        if disk is not None:
            print('Failed: Disk mounted under "%s" is already registered as disk "%s" (file %s exists).' % (mount.path, disk.name, disk.hdd_desc_file))
            exit(1)

        hdd = self.obj_by_id(self.session, name, 'name', fail_on_missing=False)
        if hdd is not None:
            print('Failed: an HDD with this name is already registered (%s).' % hdd.id)
            exit(1)

        disk = Disk(self.session, mount.path)
        disk.name = name
        disk.write()

        hdd = DBHDD(name=name)
        hdd.update_size(mount.path)
        self.session.add_all([hdd])
        self.session.commit()
        print('HDD added.')


def hash_file(f, hash_algo):
    hash_cls = getattr(hashlib, hash_algo)
    hash_obj = hash_cls()
    filesize = os.path.getsize(f)
    pb = ProgressBar(filesize)
    progress = 0
    with open(f, 'rb') as fd:
        buf = True
        while buf:
            buf = fd.read(1024 * 1024)
            hash_obj.update(buf)
            progress += len(buf)
            pb.show(progress)

    pb.clear()
    return hash_obj.hexdigest()


class UpdateCommand(Command):
    _id = 'update'
    _help = 'Disks data indexing, replication...'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.files_date_updates = []

    def _flush_date_updates(self):
        query = update(DBFile).where(DBFile.id.in_(self.files_date_updates)) \
                                .values(last_seen=self.run_date)
        self.session.execute(query)
        self.session.commit()
        self.files_date_updates = []

    def _index_file(self, hdd, root, f):
        filename = f[len(root) + 1:]
        base_filename = os.path.basename(f)
        if base_filename == HDD_CONF_FILENAME:
            return

        if base_filename == HDD_TMP_FILENAME:
            os.unlink(f)
            return

        query = select(DBFile).where(DBFile.name == filename) \
                                .where(DBFile.hdd_id == hdd.id)
        file_obj = self.session.scalars(query).first()
        created = False

        if not file_obj:
            print('Hashing file %s' % filename)
            file_obj = DBFile(name=filename, hdd_id=hdd.id)
            created = True

        if created or file_obj.deleted:
            modified_date = os.path.getmtime(f)
            file_obj.size = os.path.getsize(f)
            file_obj.hash = hash_file(f, self.cfg['main']['hash_algo'])
            file_obj.deleted = False
            file_obj.last_checked = self.run_date
            file_obj.last_seen = self.run_date

            if created and (file_obj.size != os.path.getsize(f) or modified_date != os.path.getmtime(f)):
                print(f'File "{filename}" on {hdd.name} was modified while indexing, skipping')
                return

            query = select(DBFile).where(DBFile.name == filename) \
                                    .where(DBFile.hash != file_obj.hash)
            conflicts = list(self.session.scalars(query))
            if conflicts:
                print('*** Hash conflict ***: file "%s" on %s has a different hash (%s), than already indexed file(s):' % (filename, hdd.name, file_obj.hash))
                FilesCommand.show_files(cfg, conflicts)
                print()
                print('Please, overwrite the files with faulty hashes, or delete all replicas to resolve the conflict.')
                exit(1)
            self.session.add(file_obj)
            self.session.commit()
        else:
            self.files_date_updates.append(file_obj.id)
            if len(self.files_date_updates) >= SQL_BATCH_SIZE:
                self._flush_date_updates()

    def _index(self, hdd, root, d):
        try:
            for f in sorted(os.listdir(d)):
                f = os.path.join(d, f)
                if os.path.isdir(f):
                    self._index(hdd, root, f)
                elif os.path.isfile(f):
                    self._index_file(hdd, root, f)

            if os.listdir(d) == []:
                recursive_rmdir(hdd.name, d[len(root) + 1:], d)
        except FileNotFoundError:
            # In case recursive_rmdir removes the parent
            pass
        except PermissionError:
            print('Skipping "%s" due to permissions error' % d)

    def run(self):
        query = select(DBMount)
        mounts = list(self.session.scalars(query))
        hdds = {}
        hdds_ids = []

        query = select(DBHDD)
        hdds_not_mounted = dict([(hdd.id, hdd) for hdd in self.session.scalars(query)])

        for mount in mounts:
            disk = Disk.load(self.session, mount.path)
            if disk is None:
                continue

            print('Updating files state of %s...' % disk.name)
            hdd_id = disk.db_obj().id
            hdds[hdd_id] = {
                'id': hdd_id,
                'name': disk.name,
                'path': mount.path,
                'obj': disk
            }
            hdds_ids.append(hdd_id)
            hdds_not_mounted.pop(hdd_id)
            self._index(disk.db_obj(), mount.path, mount.path)
            self._flush_date_updates()

            # Mark files to be deleted
            query = update(DBFile).where(DBFile.hdd_id == hdd_id) \
                                    .where(DBFile.last_seen < self.run_date) \
                                    .values(deleted=True)
            self.session.execute(query)
            self.session.commit()

        print('Handling stale metadata...')
        query = select(DBFile.name).where(DBFile.deleted == True).distinct()
        deleted_files = self.session.scalars(query)
        keep_created = []
        for deleted_file in deleted_files:
            query = select(DBFile).where(DBFile.name == deleted_file)
            replicas = list(self.session.scalars(query))

            hashes = set([r.hash for r in replicas if not r.deleted])
            if len(hashes) > 1:
                print('WARNING: skipping file "%s" that has different hashes on different hdd, please resolve the conflict' % deleted_file)
                continue

            renamed_to = None
            if len(hashes) > 0:
                _hash = hashes.pop()
                query = select(DBFile).where(DBFile.hash == _hash) \
                                        .where(DBFile.name != deleted_file) \
                                         .where(DBFile.deleted == False) \
                                         .where(DBFile.created == True)
                renamed_to = self.session.scalars(query).first()

            if renamed_to:
                all_renamed = True
                for replica in replicas:
                    if replica.deleted:
                        continue
                    replica_hdd = hdds.get(replica.hdd_id)
                    if not replica_hdd:
                        all_renamed = False
                        continue
                    query = select(DBFile).where(DBFile.name == renamed_to.name) \
                                                    .where(DBFile.hdd_id == replica.hdd_id)
                    target_file = self.session.scalars(query).first()
                    if target_file and not target_file.deleted:
                        print('WARNING: skipping rename of file "%s" to "%s", since target already exists on "%s"' % (deleted_file, target_file.name, replica_hdd['name']))
                        all_renamed = False
                        continue

                    if target_file and target_file.deleted:
                        self.session.delete(target_file)
                        self.session.commit()

                    print('Renaming', deleted_file, 'to', renamed_to.name, 'on', replica_hdd['name'])
                    src_file = os.path.join(replica_hdd['path'], deleted_file)
                    dst_file = os.path.join(replica_hdd['path'], renamed_to.name)
                    os.makedirs(os.path.dirname(dst_file), exist_ok=True)
                    os.rename(src_file, dst_file)
                    recursive_rmdir(replica_hdd['name'], os.path.dirname(deleted_file), os.path.dirname(src_file))

                    replica.name = renamed_to.name
                    self.session.add_all([replica])
                    self.session.commit()
                if all_renamed:
                    query = delete(DBFile).where(DBFile.name == deleted_file)
                    self.session.execute(query)
                    self.session.commit()
                else:
                    keep_created.append(renamed_to.id)
            else:
                all_deleted = True
                for replica in replicas:
                    if replica.deleted:
                        continue

                    replica_hdd = hdds.get(replica.hdd_id)
                    if not replica_hdd:
                        all_deleted = False
                        continue

                    print('Deleting', replica.name, 'from', replica_hdd['name'])
                    src_file = os.path.join(replica_hdd['path'], deleted_file)
                    if os.path.isfile(src_file):
                        os.unlink(src_file)
                    self.session.delete(replica)
                    self.session.commit()
                    dir_name = os.path.dirname(src_file)
                    recursive_rmdir(replica_hdd['name'], os.path.dirname(deleted_file), dir_name)
                if all_deleted:
                    query = delete(DBFile).where(DBFile.name == deleted_file)
                    self.session.execute(query)
                    self.session.commit()

        replicas_count = int(self.cfg['main']['replicas'])
        files_to_delete = self.session.query(DBFile.name, func.count(DBFile.name)) \
                        .group_by(DBFile.name).having(func.count(DBFile.name) > replicas_count)
        for file in files_to_delete:
            query = select(DBFile).where(DBFile.name == file.name)
            existing_files = list(self.session.scalars(query))
            all_exists = True
            for f in existing_files:
                if f.deleted:
                    all_exists = False
                    break
            if not all_exists:
                # The file is marked to be deleted but some replicas are not online to delete them
                continue

            to_delete = len(existing_files) - replicas_count

            while to_delete > 0 and len(existing_files):
                replica = existing_files.pop()

                if replica.hdd_id not in hdds:
                    continue

                hdd = hdds[replica.hdd_id]
                filepath = os.path.join(hdd['path'], file.name)

                print('Deleting', replica.name, 'from', replica.hdd.name)
                os.unlink(filepath)
                dir_name = os.path.dirname(filepath)
                recursive_rmdir(replica.hdd.name, os.path.dirname(replica.name), dir_name)

                self.session.delete(replica)
                self.session.commit()

                to_delete -= 1

        for hdd in hdds.values():
            db_hdd = hdd['obj'].db_obj()
            hdd['obj'].update_size()
            self.session.add(db_hdd)
        self.session.commit()

        print('Replicating files...')

        hdd_pair_file_counts = {} # Store the number of files that could be copied for each pair of HDD that could be plugged in
        missing_replicas_count = 0
        missing_space_count = 0

        files_to_replicate = self.session.query(DBFile.name, func.count(DBFile.name)) \
                        .group_by(DBFile.name).having(func.count(DBFile.name) < replicas_count)
        files_to_replicate_count = files_to_replicate.count()

        for file_no, file in enumerate(files_to_replicate):
            file_no_str = '%s/%s' % (str(file_no + 1).rjust(len(str(files_to_replicate_count)), ' '), files_to_replicate_count)
            query = select(DBFile).where(DBFile.name == file.name)
            existing_files = list(self.session.scalars(query))
            all_exists = True
            for f in existing_files:
                if f.deleted:
                    all_exists = False
                    break
            if not all_exists:
                # The file is marked to be deleted but some replicas are not online to delete them
                continue

            to_copy = replicas_count - len(existing_files)

            dst_hdd_ids = set(hdds_ids) - set([f.hdd_id for f in existing_files])
            online_hdd = [hdds[f.hdd_id] for f in existing_files if f.hdd_id in hdds]

            while len(online_hdd) and len(dst_hdd_ids) and to_copy > 0:
                src_hdd = online_hdd[0]
                src_file = os.path.join(src_hdd['path'], file.name)
                dst_hdd = hdds[dst_hdd_ids.pop()]
                dst_file = os.path.join(dst_hdd['path'], file.name)

                dst_hdd['obj'].update_size()
                dst_db_hdd = dst_hdd['obj'].db_obj()
                filesize = os.path.getsize(src_file)
                if dst_db_hdd.available < filesize:
                    continue

                print('Copying %s from "%s" to "%s"' % (file.name, src_hdd['name'], dst_hdd['name']))
                dir_name = os.path.dirname(dst_file)
                os.makedirs(dir_name, exist_ok=True)
                tmp_dst_file = os.path.join(dir_name, HDD_TMP_FILENAME)
                hash_cls = getattr(hashlib, self.cfg['main']['hash_algo'])
                hash_obj = hash_cls()

                pb = ProgressBar(existing_files[0].size)
                progress = 0
                with open(src_file, 'rb') as src_fd, open(tmp_dst_file, 'wb') as dst_fd:
                    buf = True
                    while buf:
                        buf = src_fd.read(1024 * 1024)
                        dst_fd.write(buf)
                        hash_obj.update(buf)
                        progress += len(buf)
                        pb.show(progress, prefix=file_no_str)
                pb.clear()

                dst_hash = hash_obj.hexdigest()
                if existing_files[0].hash != dst_hash:
                    print('Failed: The hash of "%s" (%d) on %s has changed since it was first indexed' % (file.name, existing_files[0].id, src_hdd['name']))
                    exit(1)

                os.rename(tmp_dst_file, dst_file)

                file_obj = DBFile(name=existing_files[0].name,
                                  hash=existing_files[0].hash,
                                  size=existing_files[0].size,
                                  hdd_id=dst_hdd['id'],
                                  last_seen=self.run_date,
                                  last_checked=self.run_date.date())

                dst_hdd['obj'].update_size()
                self.session.add_all([file_obj, dst_db_hdd])
                self.session.commit()
                to_copy -= 1

            if to_copy:
                potential_src = [f.hdd for f in existing_files]

                found = False
                for dst in hdds_not_mounted.values():
                    if dst in potential_src:
                        continue
                    if dst.available < existing_files[0].size:
                        continue

                    found = True
                    for src in potential_src:
                        pair = ' and '.join(sorted([src.name, dst.name]))
                        hdd_pair_file_counts[pair] = hdd_pair_file_counts.get(pair, 0) + 1

                if found:
                    missing_replicas_count += 1
                else:
                    missing_space_count += 1

        query = update(DBFile).where(DBFile.hdd_id.in_(hdds_ids)).where(DBFile.id.not_in(keep_created)).values(created=False)
        self.session.execute(query)
        self.session.commit()

        if hdd_pair_file_counts:
            print()
            print('%i files are missing replicas, some replicas could be done if the following disks are mounted together:' % missing_replicas_count)
            print()
            self.show_data(sorted(hdd_pair_file_counts.items(), key=lambda x: -x[1]), OrderedDict(((0, 'Disks'), (1, 'Pending replicas'))))

        if missing_space_count:
            print()
            print("%i files can't be copied due to missing free space." % missing_space_count)
            print('Please add a new disk or delete some files.')

        FilesCommand.files_to_check_warn(self.run_date.date(), self.session, self.cfg)

        if missing_replicas_count or missing_space_count:
            exit(1)


class FilesCommand(Command):
    _id = 'files'
    _name = 'file'
    _help = 'Files management'
    _subcommands = {
        'search': {
            'params': '<filename regexp>',
            'help': 'Search for files',
        },
        'ssearch': {
            'params': '<filename regexp>',
            'help': 'Case sensitive search for files',
        },
        'duplicates': {
            'help': 'Finds duplicates files'
        },
        'check': {
            'help': 'Check the hash of files'
        },
        'rehash': {
            'help': 'Force rehash a file',
            'params': '<file path>'
        }
    }
    _table = DBFile

    @classmethod
    def show_files(cls, cfg, files):
        hash_len = int(cfg['main']['hash_print_len'])
        for file in files:
            file.filesize = human_filesize(file.size)
            file.hash = file.hash[:hash_len]
        cls.show_tables(files, OrderedDict((('id', 'Id'), ('name', 'Filename'), ('hdd.name', 'HDD'), ('filesize', 'Size',), ('hash', cfg['main']['hash_algo'].title()))))

    def _search(self, r, sensitive):
        if sensitive:
            op = DBFile.name.op('regexp')
        else:
            r = r.lower()
            op = func.lower(DBFile.name).op('regexp')
        query = select(DBFile).filter(op(r)).where(DBFile.deleted == False).order_by(DBFile.name.asc(), DBFile.hdd_id.asc())
        files = list(self.session.scalars(query))

        self.show_files(self.cfg, files)

    def run_search(self, r):
        self._search(r, False)

    def run_ssearch(self, r):
        self._search(r, True)

    def run_duplicates(self):
        duplicate_hashes = self.session.query(DBFile.hash) \
                .group_by(DBFile.hash).having(func.count(func.distinct(DBFile.name)) > 1)
        duplicate_files = select(DBFile) \
                .where(DBFile.hash.in_(duplicate_hashes)).order_by(DBFile.hash.asc(), DBFile.name.asc(), DBFile.hdd_id.asc())
        files = list(self.session.scalars(duplicate_files))
        hash_len = int(cfg['main']['hash_print_len'])
        for file in files:
            file.hash = file.hash[:hash_len]

        self.show_tables(files, OrderedDict((('name', 'Filename'), ('hash', cfg['main']['hash_algo'].title()), ('hdd.name', 'HDD'))))

    @classmethod
    def get_disks(cls, session):
        query = select(DBMount).order_by(DBMount.path.asc())
        mounts = list(session.scalars(query))

        disks = {}
        for mount in mounts:
            disk = Disk.load(session, mount.path)
            if disk:
                disks[disk.db_obj().id] = disk
        return disks

    @classmethod
    def files_to_check(cls, run_date, session, cfg, all_hdd):
        last_check = run_date - timedelta(days=int(cfg['main']['check_days']))
        query = session.query(DBFile).where(DBFile.last_checked <= last_check)

        if not all_hdd:
            disks = cls.get_disks(session)
            disk_ids = disks.keys()
            query = query.where(DBFile.hdd_id.in_(disk_ids))
        return query

    @classmethod
    def files_to_check_warn(cls, run_date, session, cfg):
        files_to_check = cls.files_to_check(run_date, session, cfg, True)

        to_check_count = files_to_check.count()
        if to_check_count:
            last_check = run_date - timedelta(days=int(cfg['main']['check_days']))
            hdds = list(session.query(DBFile.hdd_id).where(DBFile.last_checked <= last_check).distinct())[0]
            hdds = session.query(DBHDD.name).where(DBHDD.id.in_(hdds)).all()

            hdds_str = ','.join(['"%s"' % hdd.name for hdd in hdds])
            print(f'\n** The integrity of {to_check_count} files has not been checked since more than {cfg["main"]["check_days"]} days **.\n** Mount drive{"s" if len(hdds) > 1 else ""} {hdds_str}, and run "{sys.argv[0]} files check" to update them. **')

    def run_check(self):
        files = self.files_to_check(self.run_date, self.session, self.cfg, False)
        has_errors = False
        count = 0
        disks = self.get_disks(self.session)

        for f in files:
            count += 1
            disk = disks[f.hdd_id]
            print('Hashing', f.name, 'on', disk.name)

            file_path = os.path.join(disk.mount_path, f.name)
            _hash = hash_file(file_path, self.cfg['main']['hash_algo'])
            if _hash != f.hash:
                print('WARNING: hash of file (%s) has changed (was %s)' % (_hash, f.hash))
                has_errors = True
            else:
                f.last_checked = self.run_date
                self.session.add(f)
                self.session.commit()

        print('%s files checked.' % count)
        exit(1 if has_errors else 0)

    def run_rehash(self, file_id):
        f = FilesCommand.obj_by_id(self.session, file_id)

        disks = self.get_disks(self.session)
        if f.hdd_id not in disks:
            print(f'Failed: Hdd "{f.hdd.name}" must be mounted to rehash "{f.name}"')
            exit(1)

        filepath = os.path.join(disks[f.hdd_id].mount_path, f.name)
        f.last_seen = self.run_date
        f.last_checked = self.run_date.date()
        f.size = os.path.getsize(filepath)
        f.deleted = False
        f.created = False

        print(f'Rehashing "{f.name}" on "{f.hdd.name}"')
        f.hash = hash_file(filepath, self.cfg['main']['hash_algo'])
        self.session.add(f)
        self.session.commit()
        print('Done')


class DebugCommand(Command):
    _id = 'debug'
    _help = 'Debug commands'
    _subcommands = {
        'show_file': {
            'params': '<file id>',
            'help': 'Show file',
        }
    }

    def run_show_file(self, file_id):
        f = FilesCommand.obj_by_id(self.session, file_id)
        f.filesize = human_filesize(f.size)
        self.show_obj('File ' + file_id, f, ['id', 'name', 'filesize', 'hdd.name', 'last_seen', 'last_checked', 'created', 'deleted', 'hash'])


if __name__ == '__main__':
    cfg_file = os.environ.get('HDD_REPLICAS_CONFIG_FILE', '~/.local/share/hddreplicas/config')
    cfg = ConfigParser()
    cfg['main'] = {
        'database': 'sqlite:///~/.local/share/hddreplicas/database.sqlite',
        'hash_algo': 'md5',
        'hash_print_len': '7',
        'replicas': '2',
        'check_days': '90'
    }

    cfg_file = os.path.expanduser(cfg_file)
    if os.path.exists(cfg_file):
        with open(cfg_file) as fd:
            cfg.read_file(fd)
    else:
        os.makedirs(os.path.dirname(cfg_file), exist_ok=True)
        with open(cfg_file, 'w') as fd:
            cfg.write(fd)


    hash_algo = cfg['main']['hash_algo']
    algos = []
    for a in dir(hashlib):
        try:
            b = getattr(hashlib, a)
            fake_hash = b(b'')
            if hasattr(fake_hash, 'hexdigest'):
                algos.append(a)
        except TypeError:
            pass
    if hash_algo not in algos:
        raise Exception('Invalid hash_algo "%s" config option, must be one of %s' % (hash_algo, ', '.join(sorted(algos))))

    database = cfg['main']['database']
    if database.startswith('sqlite:///~/'):
        create_dir = False
        if database.startswith('sqlite:///~/.local/share/hddreplicas/'):
            create_dir = True
        home = os.environ.get('HOME')
        full_path = os.path.join(home, database[len('sqlite:///~/'):])

        if create_dir:
            os.makedirs(os.path.dirname(full_path), exist_ok=True)

        database = 'sqlite:///' + full_path

    engine = create_engine(database, echo=False)
    Base.metadata.create_all(engine)
    Command.handle(engine, cfg)
