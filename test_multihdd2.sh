#!/bin/bash
. ./test_common.sh

mkdir -p "$TEST_DIR"/{mount1,mount2,mount3}

check "Adding mount1" 0 "Mount point added." ./hddreplicas.py mount add "$TEST_DIR"/mount1
check "Adding mount2" 0 "Mount point added." ./hddreplicas.py mount add "$TEST_DIR"/mount2
check "Adding mount3" 0 "Mount point added." ./hddreplicas.py mount add "$TEST_DIR"/mount3

check "Adding HDD1" 0 "HDD added." ./hddreplicas.py hdd add 1 HDD1
check "Adding HDD2" 0 "HDD added." ./hddreplicas.py hdd add 2 HDD2
check "Adding HDD3" 0 "HDD added." ./hddreplicas.py hdd add 3 HDD3

tar cf "$TEST_DIR"/multi.tar "$TEST_DIR"/mount{1,2,3}* "$TEST_DIR"/database.sqlite

echo file1 > "$TEST_DIR"/mount1/file1

UPDATE_LOG="Updating files state of HDD1...
Hashing file file1
Updating files state of HDD2...
Updating files state of HDD3...
Handling stale metadata...
Replicating files...
Copying file1 from \"HDD1\" to \"HDD2\""

check "Delete HDD - setup" 0 "$UPDATE_LOG" ./hddreplicas.py update
check "Delete HDD - remove HDD" 0 "HDD \"HDD1\" (1) removed." ./hddreplicas.py hdd rm 1

HDD_DEL_UPDATE_REPLICATE="Updating files state of HDD2...
Updating files state of HDD3...
Handling stale metadata...
Replicating files...
Copying file1 from \"HDD2\" to \"HDD3\""

check "Delete HDD - rereplicate" 0 "$HDD_DEL_UPDATE_REPLICATE" ./hddreplicas.py update
